var siofu = require("socketio-file-upload");
var app = require("express")();
app.use(siofu.router);
var server = require("http").Server(app);
var io = require("socket.io")(server);
var mysql = require("mysql");
const path = require("path");

var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "a",
  database: "ChatApp"
});
connection.connect();

server.listen(3000);

let Users = [];

app.get("/", function(req, res) {
  res.sendFile("src/index.html", { root: __dirname });
});
app.get("/css/index.css", function(req, res) {
  res.sendFile("src/css/index.css", { root: __dirname });
});
app.get("/js/index.js", function(req, res) {
  res.sendFile("src/js/index.js", { root: __dirname });
});
app.get("/files/:file(*)", function(req, res) {
  var file = req.params.file;
  var fileLocation = path.join(__dirname, "files", file);
  res.download(fileLocation, file);
});

io.on("connection", function(socket) {
  const uploader = new siofu();
  uploader.dir = path.join(__dirname, "files");
  uploader.listen(socket);
  uploader.on("saved", function(event) {
    console.log(event.file);
  });
  uploader.on("error", function(event) {
    console.log("Error from uploader", event);
  });

  socket.on("authentication", function(data) {
    ValidateUser(data.username, data.password).then(results => {
      if (results.status == "badPassword") socket.emit("badPassword");
      else if (results.status == "ok") {
        socket.DbId = results.result.ID;
        socket.username = data.username;
        socket.emit("authenticated");
        Users.push(data.username);
        io.emit("users", Users);
        GetHistory.then(results => {
          socket.emit("messageHistory", results);
        });
      }
    });
  });
  socket.on("register", function(data) {
    CheckUsername(data.username).then(function(result) {
      if (result) {
        socket.emit("usernameExist");
      } else {
        Register(data.username, data.password).then(function(result) {
          socket.DbId = result.insertId;
          socket.username = data.username;
          socket.emit("authenticated");
          Users.push(data.username);
          io.emit("users", Users);
        });
        GetHistory.then(results => {
          socket.emit("messageHistory", results);
        });
      }
    });
  });
  socket.on("logout", () => {
    Users = Users.filter(user => user != socket.username);
    io.emit("users", Users);
  });
  socket.on("disconnect", () => {
    Users = Users.filter(user => user != socket.username);
    io.emit("users", Users);
  });
  socket.on("sendMessage", function(msg) {
    connection.query(
      "INSERT INTO messages(fromUser, message, file) VALUES (?, ?, 0);",
      [socket.DbId, msg],
      function(error, results, fields) {
        if (error) throw error;
        io.emit("newMessage", {
          username: socket.username,
          message: msg
        });
      }
    );
  });
  socket.on("sendFile", function(msg) {
    connection.query(
      "INSERT INTO messages(fromUser, message, file) VALUES (?, ?, 1);",
      [socket.DbId, msg],
      function(error, results, fields) {
        if (error) throw error;
        io.emit("newFile", {
          username: socket.username,
          name: msg
        });
      }
    );
  });
  socket.on("changeUsername", function(username) {
    ChangeUsername(socket.username, username).then(status => {
      if (status == "ok") {
        var index = Users.indexOf(socket.username);
        socket.username = username;
        Users[index].username = username;
        io.emit("users", Users);
      }
    });
  });
});
var ChangeUsername = function(username, newUsername) {
  return new Promise(function(resolve, reject) {
    connection.query(
      "UPDATE users SET username = ? WHERE username = ?;",
      [newUsername, username],
      function(error, results, fields) {
        if (error) throw error;
        resolve("ok");
      }
    );
  });
};

var ValidateUser = function(username, password) {
  return new Promise(function(resolve, reject) {
    connection.query(
      "SELECT * FROM users WHERE username = ?;",
      [username],
      function(error, results, fields) {
        if (error) resolve({ status: "error" });
        else if (results.length > 0) {
          if (results[0].password == password)
            resolve({ status: "ok", result: results[0] });
          else resolve({ status: "badPassword" });
        }
      }
    );
  });
};

var CheckUsername = function(username) {
  return new Promise(function(resolve, reject) {
    connection.query(
      "SELECT * FROM users WHERE username = ?",
      [username],
      function(error, results, fields) {
        if (results.length > 0) resolve(true);
        else resolve(false);
      }
    );
  });
};
var Register = function(username, password) {
  return new Promise(function(resolve, reject) {
    connection.query(
      "INSERT INTO users SET ?",
      { username: username, password: password },
      function(error, results, fields) {
        if (error) reject();
        else {
          resolve(results);
        }
      }
    );
  });
};
var GetHistory = new Promise(function(resolve, reject) {
  connection.query(
    "SELECT messages.message, users.username, messages.file FROM messages INNER JOIN users ON messages.fromUser = users.ID LIMIT 50;",
    function(error, results, fields) {
      if (error) throw error;
      resolve(results);
      // socket.emit("messageHistory", results);
    }
  );
});
// function SendHistory(socket) {
//   connection.query(
//     "SELECT messages.message, users.username, messages.file FROM messages INNER JOIN users ON messages.fromUser = users.ID LIMIT 50;",
//     function(error, results, fields) {
//       if (error) throw error;
//     }
//   );
// }
