var socket = io.connect("http://localhost:3000");
var uploader = new SocketIOFileUpload(socket);
var Username = "";
var SendFile = false;
socket.on("connect", function() {});
socket.on("authenticated", function() {
  HideLogin();
  ShowChat();
});

uploader.listenOnSubmit(
  document.getElementById("sendMsg"),
  document.getElementById("siofu_input")
);

uploader.addEventListener("progress", function(event) {
  var percent = (event.bytesLoaded / event.file.size) * 100;
  console.log("File is", percent.toFixed(2), "percent loaded");
});

uploader.addEventListener("complete", function(event) {
  console.log(event.success);
  console.log(event.file);
});

socket.on("users", function(users) {
  const people = document.querySelector("#people");
  people.innerHTML =
    '<div class="selfUsername" onclick="ShowChangeUsername()">' +
    Username +
    "</div>";
  users.forEach(user => {
    people.innerHTML += '<li class="user">' + user + "</li>";
  });
});
socket.on("newMessage", function(msg) {
  const chat = document.querySelector("#chat");
  var messageClass = "message";
  if (msg.username == Username) messageClass += " self";
  chat.innerHTML +=
    '<div class="' +
    messageClass +
    '"><div class="content">' +
    msg.username +
    ": " +
    msg.message +
    "</div></div>";
  chat.scrollTo(0, chat.scrollHeight);
});
socket.on("newFile", function(msg) {
  const chat = document.querySelector("#chat");
  var messageClass = "message";
  if (msg.username == Username) messageClass += " self";
  chat.innerHTML +=
    '<a href="' +
    window.location +
    "files/" +
    msg.name +
    '"><div class="' +
    messageClass +
    '"><div class="content">' +
    msg.username +
    " FILE: " +
    msg.name +
    "</div></div></a>";
  chat.scrollTo(0, chat.scrollHeight);
});
socket.on("failedAuth", () => {
  console.log("failed to login");
});
socket.on("failedToRegister", () => {
  console.log("failed to register");
});
socket.on("usernameExist", () => {
  console.log("username exist");
});
socket.on("messageHistory", function(data) {
  const chat = document.querySelector("#chat");
  chat.innerHTML = "";
  data.forEach(msg => {
    if (msg.file)
      chat.innerHTML +=
        '<a href="' +
        window.location +
        "files/" +
        msg.message +
        '"><div class="message"><div class="content">' +
        msg.username +
        " FILE: " +
        msg.message +
        "</div></div></a>";
    else
      chat.innerHTML +=
        '<div class="message"><div class="content">' +
        msg.username +
        ": " +
        msg.message +
        "</div></div>";
  });
  chat.scrollTo(0, chat.scrollHeight);
});
function FilePrompt() {
  document.querySelector("#siofu_input").click();
}
document.querySelector("#siofu_input").onchange = function(e) {
  document.querySelector("#inputField").value = e.target.files[0].name;
  SendFile = true;
};

function SendMessage() {
  const message = document.querySelector("#inputField");
  if (SendFile) socket.emit("sendFile", message.value);
  else socket.emit("sendMessage", message.value);
  message.value = "";
}

function changeUsername() {
  const newUsername = document.querySelector("#newUsername");
  socket.emit("changeUsername", newUsername.value);
  Username = newUsername.value;
  HideChangeUsername();
}

function Register() {
  const username = document.querySelector("#username");
  const password = document.querySelector("#password");
  socket.emit("register", {
    username: username.value,
    password: password.value
  });
  Username = username.value;
}

function Logout() {
  socket.emit("logout");
  HideChat();
  ShowLogin();
}

function Login() {
  const username = document.querySelector("#username");
  const password = document.querySelector("#password");
  socket.emit("authentication", {
    username: username.value,
    password: password.value
  });
  Username = username.value;
}

function ShowChangeUsername() {
  document.querySelector(".usernameChange").style.display = "flex";
}
function HideChangeUsername() {
  document.querySelector(".usernameChange").style.display = "none";
}
function HideLogin() {
  document.querySelector("#login").style.display = "none";
}
function ShowLogin() {
  document.querySelector("#login").style.display = "flex";
}
function ShowChat() {
  document.querySelector("#messenger").style.display = "flex";
}
function HideChat() {
  document.querySelector("#messenger").style.display = "none";
}
